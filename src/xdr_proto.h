#include<rpc/xdr.h>

#ifndef MYPROTOFORC
#define MYPROTOFORC

typedef struct __st_pointer_holder{
    void *p1, *p2;
}DPHOLDER;

typedef struct __st_pb_wrapper{
    int chk;
    unsigned int slen;
    char *s;
}PBW;

bool_t xdr_proto(XDR *, PBW *);
PBW* make_pbw(char **, int);

#endif
