#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include "namenode.h"
#include "datanode.h"
#include "jobtracker.h"
#include "hdfs.pb-c.h"
#include "xdr_proto.h"

void get_list(void);
void put(const char *);
void get(const char *);
void map_reduce(const char *);

int main(int argc, char *argv[]){

    char inpcmd[102];

    while(142857){
	inpcmd[0] = '\0';
	fprintf(stdout, "$>");
	scanf("%[^\n]", inpcmd);
	getchar();
	if(strncmp(inpcmd, "list", 4) == 0){
	    get_list();
	}
	else if(strncmp(inpcmd, "get", 3) == 0){
	    get(inpcmd+4);
	}
	else if(strncmp(inpcmd, "put", 3) == 0){
	    put(inpcmd+4);
	}
	else if(strncmp(inpcmd, "mr", 2) == 0){
	    map_reduce(inpcmd + 2);
	}
	else if(strncmp(inpcmd, "exit", 4) == 0 || strncmp(inpcmd, "quit", 4) == 0){
	    fprintf(stdout, "Bye!\n");
	    break;
	}
	else{
	    fprintf(stdout, "Unknown command %s\n", inpcmd);
	}
    }
    
    return 0;
}

void get_list(void){
    CLIENT *clnt;

    clnt = clnt_create("localhost", NAMENODE, NN, "tcp");
    if(clnt == NULL){
	clnt_pcreateerror("Creating client");
	return ;
    }

    ListFilesRequest req = LIST_FILES_REQUEST__INIT;
    char *data;
    req.dirname = "GiveIt";
    data = (char *)malloc(list_files_request__get_packed_size(&req));
    list_files_request__pack(&req, (void *)data);

    PBW *back_resp;
    ListFilesResponse *resp;
    back_resp = list_1(make_pbw(&data, list_files_request__get_packed_size(&req)), clnt);
    if(back_resp == NULL){
	clnt_perror(clnt, "Didn't list back");
	return ;
    }
    resp = list_files_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
    if(resp == NULL){
	fprintf(stdout, "NULL decode\n");
	return ;
    }
    if(resp->status != 1){
	fprintf(stdout, "Bad status %d\n", resp->status);
	return ;
    }

    int i;
    for(i = 0;i < resp->n_filenames;i++){
	fprintf(stdout, "\t%s\n", resp->filenames[i]);
    }
}

void put(const char *fname){
    int fd, fid;
    fd = open(fname, O_RDONLY);
    if(fd == -1){
	fprintf(stdout, "No such file\n");
	return ;
    }

    CLIENT *clnt;

    clnt = clnt_create("localhost", NAMENODE, NN, "tcp");
    if(clnt == NULL){
	clnt_pcreateerror("Creating client");
	return ;
    }


    OpenFileRequest req = OPEN_FILE_REQUEST__INIT;
    req.filename = (char *)fname;
    req.has_forread = 1;
    req.forread = 0;
    char *req_data;
    req_data = (char *)malloc(open_file_request__get_packed_size(&req));
    open_file_request__pack(&req, (void *)req_data);
    
    PBW *back_resp;
    OpenFileResponse *resp;
    back_resp = openfile_1(make_pbw(&req_data, open_file_request__get_packed_size(&req)), clnt);
    if(back_resp == NULL){
	clnt_perror(clnt, "Didn't open back");
	close(fd);
	return ;
    }
    resp = open_file_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
    if(resp == NULL){
	fprintf(stdout, "NULL decode\n");
	close(fd);
	return ;
    }
    if(resp->status != 1){
	fprintf(stdout, "Bad status %d\n", resp->status);
	close(fd);
	return ;
    }
    fid = resp->handle;

    char read_data[MAX_BLOCK_SIZE + 1];
    // read all data and store in hdfs
    while(1){
	int sz_read;
	sz_read = read(fd, read_data, MAX_BLOCK_SIZE); // read 4kb MAX
	if(sz_read == 0){
	    fprintf(stdout, "No more blocks to write\n");
	    break;
	}

	// get blocks for writing data
	AssignBlockRequest req = ASSIGN_BLOCK_REQUEST__INIT;
	req.has_handle = 1;
	req.handle = fid;
	char *req_data;
	req_data = (char *)malloc(assign_block_request__get_packed_size(&req));
	assign_block_request__pack(&req, (void *)req_data);
    
	PBW *back_resp;
	AssignBlockResponse *resp;
	back_resp = assignblock_1(make_pbw(&req_data, assign_block_request__get_packed_size(&req)), clnt);
	if(back_resp == NULL){
	    clnt_perror(clnt, "Didn't open back");
	    close(fd);
	    return ;
	}
	resp = assign_block_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
	if(resp == NULL){
	    fprintf(stdout, "WR NULL decode\n");
	    close(fd);
	    return ;
	}
	if(resp->status != 1){
	    fprintf(stdout, "Bad status %d\n", resp->status);
	    close(fd);
	    return ;
	}
	if(resp->newblock == NULL){
	    fprintf(stdout, "NULL new block\n");
	    close(fd);
	    return ;
	}

	WriteBlockRequest wr_req = WRITE_BLOCK_REQUEST__INIT;
	wr_req.blockinfo = resp->newblock;
	wr_req.has_data = 1;
	wr_req.data.data = malloc(sizeof(char)*sz_read);
	wr_req.data.len  = sz_read; 
	memcpy(wr_req.data.data, read_data, sz_read);
	char *wr_req_data;
	wr_req_data = (char *)malloc(write_block_request__get_packed_size(&wr_req));
	write_block_request__pack(&wr_req, (void *)wr_req_data);
	PBW *tosend = make_pbw(&wr_req_data, write_block_request__get_packed_size(&wr_req));
	int i, s_cnt;
	for(i = 0, s_cnt = 0;s_cnt < REP_FACTOR && i < resp->newblock->n_locations;i++){
	    CLIENT *wr_clnt;
	    fprintf(stdout, "Contacting DN %d\n", resp->newblock->locations[i]->port);
	    wr_clnt = clnt_create("localhost", DATANODE, resp->newblock->locations[i]->port, "tcp");
	    if(clnt == NULL){
		clnt_pcreateerror("Creating wb node");
		continue;
	    }
	    
	    PBW *wr_back_resp;
	    WriteBlockResponse *wr_resp;
	    wr_back_resp = writeblock_1(tosend, wr_clnt);
	    if(wr_back_resp == NULL){
		clnt_perror(wr_clnt, "DN NULL reply");
		continue;
	    }
	    wr_resp = write_block_response__unpack(NULL, wr_back_resp->slen, (void *)wr_back_resp->s);
	    if(wr_resp == NULL){
		fprintf(stdout, "wr NULL decode\n");
		continue;
	    }
	    if(wr_resp->status != 1){
		fprintf(stdout, "wr Negative response\n");
	    }
	    else{
		s_cnt++;
	    }
	}
	if(s_cnt == 0){
	    fprintf(stdout, "FILE NOT WRITTEN\n");
	    return ;
	}
    }
}


void get(const char *fname){
    int fd, n_blok;
    CLIENT *clnt;

    clnt = clnt_create("localhost", NAMENODE, NN, "tcp");
    if(clnt == NULL){
	clnt_pcreateerror("Creating client");
	return ;
    }

    OpenFileRequest req = OPEN_FILE_REQUEST__INIT;
    req.filename = (char *)fname;
    req.has_forread = 1;
    req.forread = 1;
    char *req_data;
    req_data = (char *)malloc(open_file_request__get_packed_size(&req));
    open_file_request__pack(&req, (void *)req_data);
    
    PBW *back_resp;
    OpenFileResponse *resp;
    back_resp = openfile_1(make_pbw(&req_data, open_file_request__get_packed_size(&req)), clnt);
    if(back_resp == NULL){
	clnt_perror(clnt, "Didn't open back");
	return ;
    }
    resp = open_file_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
    if(resp == NULL){
	fprintf(stdout, "NULL decode\n");
	return ;
    }
    if(resp->status != 1){
	fprintf(stdout, "Bad status %d\n", resp->status);
	return ;
    }

    printf("File in %d blocks\n", (int)resp->n_blocknums);
    for(fd = 0;fd < resp->n_blocknums;fd++){
	printf("block ID: %d\n", resp->blocknums[fd]);
    }

    // get blocks locations
    BlockLocationRequest bl_req = BLOCK_LOCATION_REQUEST__INIT;
    bl_req.n_blocknums = resp->n_blocknums;
    bl_req.blocknums = malloc(sizeof(int)*resp->n_blocknums);
    memcpy(bl_req.blocknums, resp->blocknums, sizeof(int)*resp->n_blocknums);
    char *bl_req_data;
    bl_req_data = (char *)malloc(block_location_request__get_packed_size(&bl_req));
    block_location_request__pack(&bl_req, (void *)bl_req_data);
    
    PBW *bl_back_resp;
    BlockLocationResponse *bl_resp;
    bl_back_resp = getblocklocations_1(make_pbw(&bl_req_data, block_location_request__get_packed_size(&bl_req)), clnt);
    if(bl_back_resp == NULL){
	clnt_perror(clnt, "Didn't give block address");
	return ;
    }
    bl_resp = block_location_response__unpack(NULL, bl_back_resp->slen, (void *)bl_back_resp->s);
    if(bl_resp == NULL){
	fprintf(stdout, "BL NULL decode\n");
	return ;
    }
    if(bl_resp->status != 1){
	fprintf(stdout, "Bad status %d\n", bl_resp->status);
	return ;
    }
    
    fd = open(fname, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if(fd == -1){
	fprintf(stdout, "Couldn't create file %s\n", fname);
	return ;
    }
    for(n_blok = 0;n_blok < resp->n_blocknums;n_blok++){
	ReadBlockRequest rd_req = READ_BLOCK_REQUEST__INIT;
	rd_req.has_blocknumber = 1;
	rd_req.blocknumber = bl_resp->blocklocations[n_blok]->blocknumber;
	char *rd_req_data;
	rd_req_data = (char *)malloc(read_block_request__get_packed_size(&rd_req));
	read_block_request__pack(&rd_req, (void *)rd_req_data);
	PBW *tosend = make_pbw(&rd_req_data, read_block_request__get_packed_size(&rd_req));
	int i, s_cnt;
	for(i = 0, s_cnt = 0;i < bl_resp->blocklocations[n_blok]->n_locations;i++){
	    CLIENT *rd_clnt;
	    fprintf(stdout, "Contacting DN %d for block %d\n", bl_resp->blocklocations[n_blok]->locations[i]->port, bl_resp->blocklocations[n_blok]->blocknumber);
	    rd_clnt = clnt_create("localhost", DATANODE, bl_resp->blocklocations[n_blok]->locations[i]->port, "tcp");
	    if(rd_clnt == NULL){
		clnt_pcreateerror("Creating rd node");
		continue;
	    }
	    
	    PBW *rd_back_resp;
	    ReadBlockResponse *rd_resp;
	    rd_back_resp = readblock_1(tosend, rd_clnt);
	    if(rd_back_resp == NULL){
		clnt_perror(rd_clnt, "DN NULL reply");
		continue;
	    }
	    rd_resp = read_block_response__unpack(NULL, rd_back_resp->slen, (void *)rd_back_resp->s);
	    if(rd_resp == NULL){
		fprintf(stdout, "wr NULL decode\n");
		continue;
	    }
	    if(rd_resp->status != 1){
		fprintf(stdout, "wr Negative response\n");
	    }
	    else{
		s_cnt++;
		if(write(fd, rd_resp->data.data, rd_resp->data.len) != rd_resp->data.len){
		    fprintf(stderr, "Got data but couldn't write\n");
		}
		else{
		    fprintf(stdout, "Wrote %d bytes\n", (int)rd_resp->data.len);
		}
		break;
	    }
	}
	if(s_cnt == 0){
	    fprintf(stdout, "FILE NOT READ(BLOCK %d MISSING)\n", bl_resp->blocklocations[n_blok]->blocknumber);
	    return ;
	}
    }
}

void map_reduce(const char *s){
    int jobid;
    char mapname[102], reducername[102], inpfile[102], outfile[102];
    sscanf(s, "%s %s %s %s", mapname, reducername, inpfile, outfile);

    CLIENT *clnt;

    clnt = clnt_create("localhost", JOBTRACKER, JT, "tcp");
    if(clnt == NULL){
	clnt_pcreateerror("Creating client");
	return ;
    }
	
    JobSubmitRequest req = JOB_SUBMIT_REQUEST__INIT;
    char *data;
    req.mapname = mapname;
    req.reducername = reducername;
    req.inputfile = inpfile;
    req.outputfile = outfile;
    req.has_numreducetasks = 1;
    req.numreducetasks = 1;
    data = (char *)malloc(job_submit_request__get_packed_size(&req));
    job_submit_request__pack(&req, (void *)data);

    PBW *resp_pbw;
    JobSubmitResponse *resp;
    resp_pbw = jobsubmit_1(make_pbw(&data, job_submit_request__get_packed_size(&req)), clnt);
    if(resp_pbw == NULL){
	clnt_perror(clnt, "Null resp jobsubmit");
	return ;
    }
    resp = job_submit_response__unpack(NULL, resp_pbw->slen, (void *)resp_pbw->s);
    if(resp == NULL){
	fprintf(stdout, "NULL decode\n");
	return ;
    }
    if(resp->status != 1){
	fprintf(stdout, "Bad status %d\n", resp->status);
	return ;
    }
    jobid = resp->jobid;
    fprintf(stdout, "Job ID is %d\n", jobid);

    JobStatusRequest js_req = JOB_STATUS_REQUEST__INIT;
    char *js_data;
    PBW *js_req_pbw;
    js_req.has_jobid = 1;
    js_req.jobid = jobid;
    js_data = (char *)malloc(job_status_request__get_packed_size(&js_req));
    job_status_request__pack(&js_req, (void *)js_data);
    js_req_pbw = make_pbw(&js_data, job_status_request__get_packed_size(&js_req));

    // wait for task to complete
    while(142857){
	PBW *js_resp_pbw;
	JobStatusResponse *js_resp;
	js_resp_pbw = getjobstatus_1(js_req_pbw, clnt);
	if(js_resp_pbw == NULL){
	    clnt_perror(clnt, "Get status");
	    return ;
	}
	js_resp = job_status_response__unpack(NULL, js_resp_pbw->slen, (void *)js_resp_pbw->s);
	if(js_resp == NULL){
	    fprintf(stdout, "NULL decode\n");
	    return ;
	}
	if(js_resp->status != 1){
	    fprintf(stdout, "Bad status %d\n", js_resp->status);
	    return ;
	}
	
	if(js_resp->jobdone){
	    break;
	}
	fprintf(stdout, "Map: %d/%d\tReduce: %d/%d\n", js_resp->nummaptasksstarted, js_resp->totalmaptasks, js_resp->numreducetasksstarted, js_resp->totalreducetasks);

	sleep(3);
    }
    fprintf(stdout, "MapReduce finished\n");
}
