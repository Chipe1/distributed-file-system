#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<semaphore.h>
#include<pthread.h>
#include "namenode.h"
#include "datanode.h"
#include "jobtracker.h"
#include "hdfs.pb-c.h"
#include "xdr_proto.h"

void *reducer_func(void *);
void *mapper_func(void *);
int put_data(char *, char *, int);
int get_file(const char *);
char *mf_wc(char *, int);
char *mf_grep(char *, int);

int num_mappers, num_reducers, myID, *map_done, *map_busy, *reduce_done, *reduce_busy;
sem_t *map_sem, *reduce_sem;
MapTaskStatus *mts;
MapTaskInfo *mti;
ReduceTaskStatus *rts;
ReducerTaskInfo *rti;

int main(int argc, char *argv[]){
    int i;
    JobHeartBeatRequest hb_req = JOB_HEART_BEAT_REQUEST__INIT;
    
    if(argc < 2 || argc > 4){
	fprintf(stdout, "Usage: %s <TT ID> <Num. Mappers=1> <Num. Reducers=1>\n", argv[1]);
	exit(1);
    }
    num_mappers = 1;
    num_reducers = 1;
    if(argc >= 2){
	sscanf(argv[1], "%d", &myID);
    }
    if(argc >= 3){
	sscanf(argv[2], "%d", &num_mappers);
    }
    if(argc >= 4){
	sscanf(argv[3], "%d", &num_reducers);
    }

    hb_req.has_tasktrackerid = 1;
    hb_req.tasktrackerid = myID;
    hb_req.has_nummapslotsfree = 1;
    hb_req.has_numreduceslotsfree = 1;
    hb_req.locations = NULL;
    hb_req.n_mapstatus = num_mappers;
    hb_req.mapstatus = (MapTaskStatus **)malloc(sizeof(MapTaskStatus *)*num_mappers);
    hb_req.n_reducestatus = num_reducers;
    hb_req.reducestatus = (ReduceTaskStatus **)malloc(sizeof(ReduceTaskStatus *)*num_reducers);

    map_done = (int *)malloc(sizeof(int)*num_mappers);
    map_busy = (int *)malloc(sizeof(int)*num_mappers);
    reduce_done = (int *)malloc(sizeof(int)*num_reducers);
    reduce_busy = (int *)malloc(sizeof(int)*num_reducers);
    map_sem = (sem_t *)malloc(sizeof(sem_t)*num_mappers);
    reduce_sem = (sem_t *)malloc(sizeof(sem_t)*num_reducers);
    mts = (MapTaskStatus *)malloc(sizeof(MapTaskStatus)*num_mappers);
    mti = (MapTaskInfo *)malloc(sizeof(MapTaskInfo)*num_mappers);
    rts = (ReduceTaskStatus *)malloc(sizeof(ReduceTaskStatus)*num_reducers);
    rti = (ReducerTaskInfo *)malloc(sizeof(ReducerTaskInfo)*num_reducers);

    MapTaskStatus t_mts = MAP_TASK_STATUS__INIT;
    MapTaskInfo t_mti = MAP_TASK_INFO__INIT;
    ReduceTaskStatus t_rts = REDUCE_TASK_STATUS__INIT;
    ReducerTaskInfo t_rti = REDUCER_TASK_INFO__INIT;
    
    for(i = 0;i < num_mappers;i++){
	pthread_t tid;
	map_busy[i] = 0;
	mts[i] = t_mts;
	mti[i] = t_mti;
	sem_init(map_sem + i, 0, 0);
	pthread_create(&tid, NULL, mapper_func, (void *)(long long)i);
	pthread_detach(tid);
    }
    for(i = 0;i < num_reducers;i++){
	pthread_t tid;
	reduce_busy[i] = 0;
	rts[i] = t_rts;
	rti[i] = t_rti;
	sem_init(reduce_sem + i, 0, 0);
	pthread_create(&tid, NULL, reducer_func, (void *)(long long)i);
	pthread_detach(tid);
    }

    fprintf(stdout, "Started TT(%d) with %d mappers and %d reducers\n", myID, num_mappers, num_reducers);
    
    while(142857){
	hb_req.nummapslotsfree = 0;
	hb_req.numreduceslotsfree = 0;
	hb_req.n_mapstatus = 0;
	hb_req.n_reducestatus = 0;
	for(i = 0;i < num_mappers;i++){
	    if(map_busy[i] && map_done[i]){
		map_done[i] = 0;
		map_busy[i] = 0;
		hb_req.mapstatus[hb_req.n_mapstatus++] = mts + i;
		fprintf(stdout, "Mapper %d finished\n", i);
	    }
	    if(!map_busy[i]){
		hb_req.nummapslotsfree++;
	    }
	}
	for(i = 0;i < num_reducers;i++){
	    if(reduce_busy[i] && reduce_done[i]){
		reduce_done[i] = 0;
		reduce_busy[i] = 0;
		hb_req.reducestatus[hb_req.n_reducestatus++] = rts + i;
		fprintf(stdout, "Reducer %d finished\n", i);
	    }
	    if(!reduce_busy[i]){
		hb_req.numreduceslotsfree++;
	    }
	}

	CLIENT *clnt;

	clnt = clnt_create("localhost", JOBTRACKER, JT, "tcp");
	if(clnt == NULL){
	    clnt_pcreateerror("Creating client");
	    sleep(3);
	    continue;
	}
	
	char *req_data;
	PBW *resp_pbw;
	JobHeartBeatResponse *resp;
	req_data = malloc(job_heart_beat_request__get_packed_size(&hb_req));
	job_heart_beat_request__pack(&hb_req, (void *)req_data);
	resp_pbw = jobheartbeat_1(make_pbw(&req_data, job_heart_beat_request__get_packed_size(&hb_req)), clnt);
	if(resp_pbw == NULL){
	    fprintf(stdout, "JT NuLL reply\n");
	    sleep(3);
	    continue;
	}
	resp = job_heart_beat_response__unpack(NULL, resp_pbw->slen, (void *)resp_pbw->s);
	if(resp == NULL){
	    fprintf(stdout, "NULL decode\n");
	    sleep(3);
	    continue;
	}
	if(resp->status != 1){
	    fprintf(stdout, "Bad return status\n");
	    sleep(3);
	    continue;
	}
	int j;
	for(j = 0;j < resp->n_maptasks;j++){
	    for(i = 0;map_busy[i] && i < num_mappers;i++);
	    if(i == num_mappers){
		fprintf(stderr, "Too much stress from server. SUICIDE\n");
		exit(1);
	    }
	    mti[i] = *(resp->maptasks[i]);
	    map_done[i] = 0;
	    map_busy[i] = 1;
	    sem_post(map_sem + i);
	}
	for(j = 0;j < resp->n_reducetasks;j++){
	    for(i = 0;reduce_busy[i] && i < num_reducers;i++);
	    if(i == num_reducers){
		fprintf(stderr, "Too much stress from server. SUICIDE\n");
		exit(1);
	    }
	    rti[i] = *(resp->reducetasks[i]);
	    reduce_done[i] = 0;
	    reduce_busy[i] = 1;
	    sem_post(reduce_sem + i);
	}
	    
	sleep(3);
    }

    return 0;
}


void *mapper_func(void * arg){
    int map_id;
    map_id = (int)(long long)arg;

    // mapping loop
    while(142857){
	int i, map_inp_n;
	BlockLocations blk;
	char *map_inp, *map_out, map_out_name[102];
	sem_wait(map_sem + map_id);
	fprintf(stdout, "Mapper %d got Mapper(%d, %d)\n", map_id, mti[map_id].jobid, mti[map_id].taskid);
	map_done[map_id] = 1;
	blk = *mti[map_id].inputblocks;

	ReadBlockRequest rd_req = READ_BLOCK_REQUEST__INIT;
	rd_req.has_blocknumber = 1;
	rd_req.blocknumber = blk.blocknumber;
	char *rd_req_data;
	rd_req_data = (char *)malloc(read_block_request__get_packed_size(&rd_req));
	read_block_request__pack(&rd_req, (void *)rd_req_data);
	PBW *tosend = make_pbw(&rd_req_data, read_block_request__get_packed_size(&rd_req));
	
	for(i = 0;i < blk.n_locations;i++){
	    CLIENT *rd_clnt;
	    fprintf(stdout, "Contacting DN %d for block %d\n", blk.locations[i]->port, blk.blocknumber);
	    rd_clnt = clnt_create("localhost", DATANODE, blk.locations[i]->port, "tcp");
	    if(rd_clnt == NULL){
		clnt_pcreateerror("Creating DN clnt");
		continue;
	    }
	    
	    PBW *rd_back_resp;
	    ReadBlockResponse *rd_resp;
	    rd_back_resp = readblock_1(tosend, rd_clnt);
	    if(rd_back_resp == NULL){
		clnt_perror(rd_clnt, "DN NULL reply");
		continue;
	    }
	    rd_resp = read_block_response__unpack(NULL, rd_back_resp->slen, (void *)rd_back_resp->s);
	    if(rd_resp == NULL){
		fprintf(stdout, "wr NULL decode\n");
		continue;
	    }
	    if(rd_resp->status != 1){
		fprintf(stdout, "wr Negative response\n");
	    }
	    else{
		map_inp = (char *)rd_resp->data.data;
		map_inp_n = rd_resp->data.len;
		break;
	    }
	}
	if(i == blk.n_locations){
	    fprintf(stderr, "Couldn't find Block %d anywhere. Bye Bye\n", blk.blocknumber);
	    break;
	}

	if(strncmp(mti[map_id].mapname, "wc", 2) == 0){
	    map_out = mf_wc(map_inp, map_inp_n);
	}
	else if(strncmp(mti[map_id].mapname, "grep", 4) == 0){
	    map_out = mf_grep(map_inp, map_inp_n);
	}
	else{
	    fprintf(stdout, "Unknown mapping function %s\n", mti[map_id].mapname);
	    map_out = "0";
	}

	int ret_val;
	sprintf(map_out_name, "job_%d_task_%d", mti[map_id].jobid, mti[map_id].taskid);
	ret_val = put_data(map_out_name, map_out, strlen(map_out));
	if(ret_val == 0){
	    fprintf(stderr, "Couldn't store map output\n");
	    break;
	}

	mts[map_id].has_jobid = 1;
	mts[map_id].jobid = mti[map_id].jobid;
	mts[map_id].has_taskid = 1;
	mts[map_id].taskid = mti[map_id].taskid;
	mts[map_id].has_taskcompleted = 1;
	mts[map_id].taskcompleted = 1;
	mts[map_id].mapoutputfile = map_out_name;

	map_done[map_id] = 1;
    }
    
    fprintf(stderr, "Mapper %d resign\n", map_id);
    return NULL;
}

void *reducer_func(void * arg){
    int reducer_id;
    reducer_id = (int)(long long)arg;
    
    while(142857){
	int i;
	sem_wait(reduce_sem + reducer_id);
	fprintf(stdout, "Reducer %d for Reduce(%d)\n", reducer_id, rti[reducer_id].jobid);
	
	// get the mapped files
	for(i = 0;i < rti[reducer_id].n_mapoutputfiles;i++){
	    if(get_file(rti[reducer_id].mapoutputfiles[i])){
		fprintf(stdout, "Got fetched file %s\n", rti[reducer_id].mapoutputfiles[i]);
	    }
	    else{
		fprintf(stdout, "Unable to get file %s\n", rti[reducer_id].mapoutputfiles[i]);
		break;
	    }
	}
	if(i != rti[reducer_id].n_mapoutputfiles){
	    fprintf(stdout, "Failed to fetch one or more files. Undefined behaiour\n");
	    break;
	}

	// reduce step
	char red_out[MAX_BLOCK_SIZE];
	int red_out_n;
	// TODO modularize the reduction
	if(strncmp(rti[reducer_id].reducername, "wc", 2) == 0){
	    int wc_out;
	    for(i = 0, wc_out = 0;i < rti[reducer_id].n_mapoutputfiles;i++){
		FILE *fd;
		int twc;
		fd = fopen(rti[reducer_id].mapoutputfiles[i], "r");
		fscanf(fd, "%d", &twc);
		wc_out += twc;
	    }

	    sprintf(red_out, "%d\n", wc_out);
	    red_out_n = strlen(red_out);
	}
	else if(strncmp(rti[reducer_id].reducername, "grep", 4) == 0){
	    fprintf(stderr, "UNIMPLEMENTED\n");
	    break;
	}

	// store output in hdfs
	if(!put_data(rti[reducer_id].outputfile, red_out, red_out_n)){
	    fprintf(stdout, "Unable to write reducer output to %s\n", rti[reducer_id].outputfile);
	}


	rts[reducer_id].has_jobid = 1;
	rts[reducer_id].jobid = mti[reducer_id].jobid;
	rts[reducer_id].has_taskid = 1;
	rts[reducer_id].taskid = mti[reducer_id].taskid;
	rts[reducer_id].has_taskcompleted = 1;
	rts[reducer_id].taskcompleted = 1;

	reduce_done[reducer_id] = 1;
    }

    fprintf(stderr, "Reducer %d resign\n", reducer_id);
    return NULL;
}

int put_data(char *fname, char *data, int len){
    CLIENT *clnt;
    int fid;

    clnt = clnt_create("localhost", NAMENODE, NN, "tcp");
    if(clnt == NULL){
	clnt_pcreateerror("Creating client");
	return 0;
    }


    OpenFileRequest req = OPEN_FILE_REQUEST__INIT;
    req.filename = fname;
    req.has_forread = 1;
    req.forread = 0;
    char *req_data;
    req_data = (char *)malloc(open_file_request__get_packed_size(&req));
    open_file_request__pack(&req, (void *)req_data);
    
    PBW *back_resp;
    OpenFileResponse *resp;
    back_resp = openfile_1(make_pbw(&req_data, open_file_request__get_packed_size(&req)), clnt);
    if(back_resp == NULL){
	clnt_perror(clnt, "Didn't open back");
	return 0;
    }
    resp = open_file_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
    if(resp == NULL){
	fprintf(stdout, "NULL decode\n");
	return 0;
    }
    if(resp->status != 1){
	fprintf(stdout, "Bad status %d\n", resp->status);
	return 0;
    }
    fid = resp->handle;

    int read_start;
    char read_data[MAX_BLOCK_SIZE + 1];
    read_start = 0;
    while(1){
	int sz_read;
	sz_read = len - read_start;
	if(sz_read > MAX_BLOCK_SIZE){
	    sz_read = MAX_BLOCK_SIZE;
	}
	memcpy(read_data, data + read_start, sz_read);
	read_start += sz_read;
	if(sz_read == 0){
	    fprintf(stdout, "No more blocks to write\n");
	    break;
	}

	// get blocks for writing data
	AssignBlockRequest req = ASSIGN_BLOCK_REQUEST__INIT;
	req.has_handle = 1;
	req.handle = fid;
	char *req_data;
	req_data = (char *)malloc(assign_block_request__get_packed_size(&req));
	assign_block_request__pack(&req, (void *)req_data);
    
	PBW *back_resp;
	AssignBlockResponse *resp;
	back_resp = assignblock_1(make_pbw(&req_data, assign_block_request__get_packed_size(&req)), clnt);
	if(back_resp == NULL){
	    clnt_perror(clnt, "Didn't open back");
	    return 0;
	}
	resp = assign_block_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
	if(resp == NULL){
	    fprintf(stdout, "WR NULL decode\n");
	    return 0;
	}
	if(resp->status != 1){
	    fprintf(stdout, "Bad status %d\n", resp->status);
	    return 0;
	}
	if(resp->newblock == NULL){
	    fprintf(stdout, "NULL new block\n");
	    return 0;
	}

	WriteBlockRequest wr_req = WRITE_BLOCK_REQUEST__INIT;
	wr_req.blockinfo = resp->newblock;
	wr_req.has_data = 1;
	wr_req.data.data = malloc(sizeof(char)*sz_read);
	wr_req.data.len  = sz_read; 
	memcpy(wr_req.data.data, read_data, sz_read);
	char *wr_req_data;
	wr_req_data = (char *)malloc(write_block_request__get_packed_size(&wr_req));
	write_block_request__pack(&wr_req, (void *)wr_req_data);
	PBW *tosend = make_pbw(&wr_req_data, write_block_request__get_packed_size(&wr_req));
	int i, s_cnt;
	for(i = 0, s_cnt = 0;s_cnt < REP_FACTOR && i < resp->newblock->n_locations;i++){
	    CLIENT *wr_clnt;
	    fprintf(stdout, "Contacting DN %d\n", resp->newblock->locations[i]->port);
	    wr_clnt = clnt_create("localhost", DATANODE, resp->newblock->locations[i]->port, "tcp");
	    if(clnt == NULL){
		clnt_pcreateerror("Creating wb node");
		continue;
	    }
	    
	    PBW *wr_back_resp;
	    WriteBlockResponse *wr_resp;
	    wr_back_resp = writeblock_1(tosend, wr_clnt);
	    if(wr_back_resp == NULL){
		clnt_perror(wr_clnt, "DN NULL reply");
		continue;
	    }
	    wr_resp = write_block_response__unpack(NULL, wr_back_resp->slen, (void *)wr_back_resp->s);
	    if(wr_resp == NULL){
		fprintf(stdout, "wr NULL decode\n");
		continue;
	    }
	    if(wr_resp->status != 1){
		fprintf(stdout, "wr Negative response\n");
	    }
	    else{
		s_cnt++;
	    }
	}
	if(s_cnt == 0){
	    fprintf(stdout, "FILE NOT WRITTEN\n");
	    return 0;
	}
    }
    return 1;
}

int get_file(const char *fname){
    int fd, n_blok;
    CLIENT *clnt;

    clnt = clnt_create("localhost", NAMENODE, NN, "tcp");
    if(clnt == NULL){
	clnt_pcreateerror("Creating client");
	return 0;
    }

    OpenFileRequest req = OPEN_FILE_REQUEST__INIT;
    req.filename = (char *)fname;
    req.has_forread = 1;
    req.forread = 1;
    char *req_data;
    req_data = (char *)malloc(open_file_request__get_packed_size(&req));
    open_file_request__pack(&req, (void *)req_data);
    
    PBW *back_resp;
    OpenFileResponse *resp;
    back_resp = openfile_1(make_pbw(&req_data, open_file_request__get_packed_size(&req)), clnt);
    if(back_resp == NULL){
	clnt_perror(clnt, "Didn't open back");
	return 0;
    }
    resp = open_file_response__unpack(NULL, back_resp->slen, (void *)back_resp->s);
    if(resp == NULL){
	fprintf(stdout, "NULL decode\n");
	return 0;
    }
    if(resp->status != 1){
	fprintf(stdout, "Bad status %d\n", resp->status);
	return 0;
    }

    printf("File in %d blocks\n", (int)resp->n_blocknums);
    for(fd = 0;fd < resp->n_blocknums;fd++){
	printf("block ID: %d\n", resp->blocknums[fd]);
    }

    // get blocks locations
    BlockLocationRequest bl_req = BLOCK_LOCATION_REQUEST__INIT;
    bl_req.n_blocknums = resp->n_blocknums;
    bl_req.blocknums = malloc(sizeof(int)*resp->n_blocknums);
    memcpy(bl_req.blocknums, resp->blocknums, sizeof(int)*resp->n_blocknums);
    char *bl_req_data;
    bl_req_data = (char *)malloc(block_location_request__get_packed_size(&bl_req));
    block_location_request__pack(&bl_req, (void *)bl_req_data);
    
    PBW *bl_back_resp;
    BlockLocationResponse *bl_resp;
    bl_back_resp = getblocklocations_1(make_pbw(&bl_req_data, block_location_request__get_packed_size(&bl_req)), clnt);
    if(bl_back_resp == NULL){
	clnt_perror(clnt, "Didn't give block address");
	return 0;
    }
    bl_resp = block_location_response__unpack(NULL, bl_back_resp->slen, (void *)bl_back_resp->s);
    if(bl_resp == NULL){
	fprintf(stdout, "BL NULL decode\n");
	return 0;
    }
    if(bl_resp->status != 1){
	fprintf(stdout, "Bad status %d\n", bl_resp->status);
	return 0;
    }
    
    fd = open(fname, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if(fd == -1){
	fprintf(stdout, "Couldn't create file %s\n", fname);
	return 0;
    }
    for(n_blok = 0;n_blok < resp->n_blocknums;n_blok++){
	ReadBlockRequest rd_req = READ_BLOCK_REQUEST__INIT;
	rd_req.has_blocknumber = 1;
	rd_req.blocknumber = bl_resp->blocklocations[n_blok]->blocknumber;
	char *rd_req_data;
	rd_req_data = (char *)malloc(read_block_request__get_packed_size(&rd_req));
	read_block_request__pack(&rd_req, (void *)rd_req_data);
	PBW *tosend = make_pbw(&rd_req_data, read_block_request__get_packed_size(&rd_req));
	int i, s_cnt;
	for(i = 0, s_cnt = 0;i < bl_resp->blocklocations[n_blok]->n_locations;i++){
	    CLIENT *rd_clnt;
	    fprintf(stdout, "Contacting DN %d for block %d\n", bl_resp->blocklocations[n_blok]->locations[i]->port, bl_resp->blocklocations[n_blok]->blocknumber);
	    rd_clnt = clnt_create("localhost", DATANODE, bl_resp->blocklocations[n_blok]->locations[i]->port, "tcp");
	    if(rd_clnt == NULL){
		clnt_pcreateerror("Creating rd node");
		continue;
	    }
	    
	    PBW *rd_back_resp;
	    ReadBlockResponse *rd_resp;
	    rd_back_resp = readblock_1(tosend, rd_clnt);
	    if(rd_back_resp == NULL){
		clnt_perror(rd_clnt, "DN NULL reply");
		continue;
	    }
	    rd_resp = read_block_response__unpack(NULL, rd_back_resp->slen, (void *)rd_back_resp->s);
	    if(rd_resp == NULL){
		fprintf(stdout, "wr NULL decode\n");
		continue;
	    }
	    if(rd_resp->status != 1){
		fprintf(stdout, "wr Negative response\n");
	    }
	    else{
		s_cnt++;
		if(write(fd, rd_resp->data.data, rd_resp->data.len) != rd_resp->data.len){
		    fprintf(stderr, "Got data but couldn't write\n");
		}
		else{
		    fprintf(stdout, "Wrote %d bytes\n", (int)rd_resp->data.len);
		}
		break;
	    }
	}
	if(s_cnt == 0){
	    fprintf(stdout, "FILE NOT READ(BLOCK %d MISSING)\n", bl_resp->blocklocations[n_blok]->blocknumber);
	    return 0;
	}
    }
    return 1;
}


char *mf_wc(char *data, int len){
    char *toret;
    toret = (char *)malloc(sizeof(char)*102);
    sprintf(toret, "%d\n", len);
    return toret;
}

char *mf_grep(char *data, int len){
    fprintf(stderr, "UNIMPLEMENTED");
    return NULL;
}
