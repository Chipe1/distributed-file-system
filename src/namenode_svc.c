/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "datanode.h"
#include "namenode.h"
#include<stdio.h>
#include<stdlib.h>
#include<rpc/pmap_clnt.h>
#include<string.h>
#include<memory.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<unistd.h>
#include<pthread.h>
#include "hdfs.pb-c.h"

#ifndef SIG_PF
#define SIG_PF void(*)(int)
#endif

int find_file(const char *);
void save_state(void);
void load_state(void);
BlockLocations *find_block(int);

int n_files, n_fileblocks[MAX_FILES], fileblocks[MAX_FILES][MAX_BLK], fl_block[MAX_BLK], fl_dn[MAX_DN], dnblocks[MAX_DN][MAX_BLK];
char filenames[MAX_FILES][100];
DataNodeLocation dnloc[MAX_DN];

void* namenode_t_func(void *t_arg){
    struct svc_req *rqstp;
    register SVCXPRT *transp;

    rqstp = (struct svc_req *)(((DPHOLDER *)t_arg)->p1);
    transp = (SVCXPRT *)(((DPHOLDER *)t_arg)->p2);

    union {
	PBW openfile_1_arg;
	PBW getblocklocations_1_arg;
	PBW assignblock_1_arg;
	PBW closefile_1_arg;
	PBW list_1_arg;
	PBW sendblockreport_1_arg;
	PBW sendheartbeat_1_arg;
    } argument;
    char *result;
    xdrproc_t _xdr_argument, _xdr_result;
    char *(*local)(char *, struct svc_req *);

    switch (rqstp->rq_proc) {
    case NULLPROC:
	(void) svc_sendreply (transp, (xdrproc_t) xdr_void, (char *)NULL);
	return NULL;

    case openFile:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) openfile_1_svc;
	break;

    case getBlockLocations:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) getblocklocations_1_svc;
	break;

    case assignBlock:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) assignblock_1_svc;
	break;

    case closeFile:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) closefile_1_svc;
	break;

    case list:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) list_1_svc;
	break;

    case sendBlockReport:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) sendblockreport_1_svc;
	break;

    case sendHeartBeat:
	_xdr_argument = (xdrproc_t) xdr_proto;
	_xdr_result = (xdrproc_t) xdr_proto;
	local = (char *(*)(char *, struct svc_req *)) sendheartbeat_1_svc;
	break;

    default:
	svcerr_noproc (transp);
	return NULL;
    }
    memset ((char *)&argument, 0, sizeof (argument));
    if (!svc_getargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
	svcerr_decode (transp);
	return NULL;
    }
    result = (*local)((char *)&argument, rqstp);
    if (result != NULL && !svc_sendreply(transp, (xdrproc_t) _xdr_result, result)) {
	svcerr_systemerr (transp);
    }
    if (!svc_freeargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
	fprintf (stderr, "%s", "unable to free arguments");
	exit (1);
    }

    return NULL;
}

static void namenode_1(struct svc_req *rqstp, SVCXPRT *transp){
    DPHOLDER *t_arg;
    pthread_t  tid;
    
    t_arg = (DPHOLDER *)malloc(sizeof(DPHOLDER));
    t_arg->p1 = (void *)rqstp;
    t_arg->p2 = (void *)transp;

    pthread_create(&tid, NULL, namenode_t_func, (void *)t_arg);
    //    pthread_detach(tid);
    pthread_join(tid, NULL);

    return;
}

int main (int argc, char **argv){
    register SVCXPRT *transp;

    pmap_unset (NAMENODE, NN);

    transp = svcudp_create(RPC_ANYSOCK);
    if (transp == NULL) {
	fprintf (stderr, "%s", "cannot create udp service.");
	exit(1);
    }
    if (!svc_register(transp, NAMENODE, NN, namenode_1, IPPROTO_UDP)) {
	fprintf (stderr, "%s", "unable to register (NAMENODE, NN, udp).");
	exit(1);
    }

    transp = svctcp_create(RPC_ANYSOCK, 0, 0);
    if (transp == NULL) {
	fprintf (stderr, "%s", "cannot create tcp service.");
	exit(1);
    }
    if (!svc_register(transp, NAMENODE, NN, namenode_1, IPPROTO_TCP)) {
	fprintf (stderr, "%s", "unable to register (NAMENODE, NN, tcp).");
	exit(1);
    }

    load_state();

    fprintf(stdout, "Starting NN\n");

    svc_run();
    fprintf(stderr, "%s", "svc_run returned");
    exit(1);
    /* NOTREACHED */
}

PBW *openfile_1_svc(PBW *argp, struct svc_req *rqstp){
    char **toret;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved \n");
	return NULL;
    }

    // decode request
    OpenFileRequest* req;
    req = open_file_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL){
	fprintf(stderr, "error unpacking \n");
	return NULL;
    }

    OpenFileResponse resp = OPEN_FILE_RESPONSE__INIT;
    if(req->forread == 0){
	// write mode
	int fileid;

	fprintf(stdout, "Write request for %s\n", req->filename);
	if(find_file(req->filename) != -1){
	    fprintf(stdout, "Creating %s already exists\n", req->filename);
	    return NULL;
	}
	// add file to entry
	fileid = n_files++;
	strncpy(filenames[fileid], req->filename, 99);
	filenames[fileid][99] = '\0';
	n_fileblocks[fileid] = 0;

	// save state
	save_state();

	resp.has_status = 1;
	resp.status = 1;
	resp.has_handle = 1;
	resp.handle = fileid;
	resp.n_blocknums = 0;
	resp.blocknums = NULL;
    }
    else if(req->forread == 1){
	// read mode
	int fileid;

	fprintf(stdout, "Read request for %s\n", req->filename);
	fileid = find_file(req->filename);
	if(fileid == -1){
	    fprintf(stdout, "Couldn't find file %s\n", req->filename);
	    return NULL;
	}
	
	resp.has_status = 1;
	resp.status = 1;
	resp.has_handle = 0;
	resp.n_blocknums = n_fileblocks[fileid];
	resp.blocknums = malloc(sizeof(int)*n_fileblocks[fileid]);
	memcpy(resp.blocknums, fileblocks[fileid], sizeof(int)*n_fileblocks[fileid]);
    }
    else{
	fprintf(stderr, "Cannot understand open request type %d\n", (int)(req->forread));
	return NULL;
    }
    
    // return message
    toret = (char **)malloc(sizeof(char *));
    int packsize = open_file_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    open_file_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

PBW *getblocklocations_1_svc(PBW *argp, struct svc_req *rqstp){
    char **toret;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved blocklocation\n");
	return NULL;
    }

    // decode request
    BlockLocationRequest* req;
    req = block_location_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL){
	fprintf(stderr, "error unpacking block loc req\n");
	return NULL;
    }

    BlockLocationResponse resp = BLOCK_LOCATION_RESPONSE__INIT;
    int i;
    resp.has_status = 1;
    resp.status = 1;
    resp.n_blocklocations = req->n_blocknums;
    resp.blocklocations = malloc(sizeof(BlockLocations *)*req->n_blocknums);
    for(i = 0;i < req->n_blocknums;i++){
	resp.blocklocations[i] = find_block(req->blocknums[i]);
    }

    // return message
    toret = (char **)malloc(sizeof(char *));
    int packsize = block_location_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    block_location_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

PBW *assignblock_1_svc(PBW *argp, struct svc_req *rqstp){
    char **toret;
    int fid;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved assign block\n");
	return NULL;
    }

    // decode request
    AssignBlockRequest* req;
    req = assign_block_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL){
	fprintf(stderr, "error unpacking assign block\n");
	return NULL;
    }
    fid = req->handle;

    AssignBlockResponse resp = ASSIGN_BLOCK_RESPONSE__INIT;
    if(fid >= n_files){
	fprintf(stdout, "Assign to bad file\n");
	return NULL;
    }
    // find 2 random blocks
    int rdn1, rdn2, i, rstart, rblock;
    rstart = random()%MAX_DN;
    for(i = 0, rdn1 = -1, rdn2 = -1;i < (MAX_DN * MAX_DN);i++){
	if(fl_dn[i%MAX_DN]){
	    if(rstart > 0){
		rstart--;
	    }
	    else{
		if(rdn1 == -1){
		    rdn1 = i%MAX_DN;
		}
		else{
		    rdn2 = i%MAX_DN;
		    break;
		}
	    }
	}
    }
    if(rdn2 == rdn1){
	rdn2 = -1;
    }
    for(i = 0;i < MAX_BLK;i++){
	if(fl_block[i] == 0){
	    rblock = i;
	    break;
	}
    }
    if(i == MAX_BLK){
	fprintf(stderr, "OUT OF BLOCKSS!!!\n");
	return NULL;
    }
    if(rdn1 == -1){
	// no DN online
	fprintf(stdout, "No DN for assign block\n");
	resp.has_status = 1;
	resp.status = 0;
	resp.newblock = NULL;
    }
    else{
	fprintf(stdout, "Assigned BL%d to File %d\n", rblock, fid);
	fileblocks[fid][n_fileblocks[fid]++] = rblock;
	resp.has_status = 1;
	resp.status = 1;
	fl_block[rblock] = 1;
	dnblocks[rdn1][rblock] = 1;
	if(rdn2 != -1){
	    dnblocks[rdn2][rblock] = 1;
	}
	resp.newblock = find_block(rblock);

	// save state
	save_state();
    }
	
    // return message
    toret = (char **)malloc(sizeof(char *));
    int packsize = assign_block_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    assign_block_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

PBW *closefile_1_svc(PBW *argp, struct svc_req *rqstp){
    char **toret;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved closefile\n");
	return NULL;
    }

    // decode request
    CloseFileRequest* req;
    req = close_file_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL){
	fprintf(stderr, "error unpacking close file\n");
	return NULL;
    }

    //do studd
    // return message
    CloseFileResponse resp = CLOSE_FILE_RESPONSE__INIT;
    toret = (char **)malloc(sizeof(char *));
    int packsize = close_file_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    close_file_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

PBW *list_1_svc(PBW *argp, struct svc_req *rqstp){
    char **toret;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved list\n");
	return NULL;
    }

    // decode request
    ListFilesRequest* req;
    req = list_files_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL){
	fprintf(stderr, "error unpacking list file\n");
	return NULL;
    }

    ListFilesResponse resp = LIST_FILES_RESPONSE__INIT;
    int i;
    resp.has_status = 1;
    resp.status = 1;
    resp.n_filenames = n_files;
    resp.filenames = malloc(sizeof(char *)*n_files);
    // TODO try to link resp.filenames directly to global filenames
    for(i = 0;i < n_files;i++){
	resp.filenames[i] = filenames[i];
    }
    
    // return message
    toret = (char **)malloc(sizeof(char *));
    int packsize = list_files_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    list_files_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

PBW *sendblockreport_1_svc(PBW *argp, struct svc_req *rqstp){
    char **toret;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved blockreport\n");
	return NULL;
    }

    // decode the request
    BlockReportRequest *req;
    req = block_report_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL || req->location == NULL){
	fprintf(stderr, "error unpacking br request\n");
	return NULL;
    }
    fprintf(stdout, "BR from %d\n", req->id);

    BlockReportResponse resp = BLOCK_REPORT_RESPONSE__INIT;
    int i;
    if(fl_dn[req->id] == 0){
	fl_dn[req->id] = 1;
	dnloc[req->id] = *(req->location);
	fprintf(stdout, "Registered %d on (%s, %d)\n", req->id, dnloc[req->id].ip, dnloc[req->id].port);
    }
    resp.n_status = req->n_blocknumbers;
    resp.status = malloc(sizeof(int)*resp.n_status);

    // change data in memory
    for(i = 0;i < MAX_BLK;i++){
	dnblocks[req->id][i] = 0;
    }
    
    for(i = 0;i < resp.n_status;i++){
	if(fl_block[req->blocknumbers[i]] == 0){
	    resp.status[i] = 0;
	}
	else{
	    resp.status[i] = 1;
	    dnblocks[req->id][req->blocknumbers[i]] = 1;
	}
    }

    // return success
    toret = (char **)malloc(sizeof(char *));
    int packsize = block_report_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    block_report_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

PBW *sendheartbeat_1_svc(PBW *argp, struct svc_req *rqstp){
    char** toret;

    if(argp == NULL){
	fprintf(stderr, "NULL recieved heartbeat\n");
	return NULL;
    }

    // decode the request
    HeartBeatRequest *req;
    req = heart_beat_request__unpack(NULL, argp->slen, (void *)argp->s);
    if(req == NULL){
	fprintf(stderr, "error unpacking hb request\n");
	return NULL;
    }
    //    fprintf(stdout, "HB from %d\n", req->id);

    // return success
    HeartBeatResponse resp = HEART_BEAT_RESPONSE__INIT;
    resp.has_status = 1;
    resp.status = 1;
    toret = (char **)malloc(sizeof(char *));
    int packsize = heart_beat_response__get_packed_size(&resp);
    *toret = (char *)malloc(packsize);
    heart_beat_response__pack(&resp, (void *)*toret);
    
    return make_pbw(toret, packsize);
}

int find_file(const char *s){
    int i;
    for(i = 0;i < n_files;i++){
	if(strncmp(filenames[i], s, 100) == 0){
	    return i;
	}
    }
    return -1;
}

BlockLocations *find_block(int blocknum){
    BlockLocations *toret;
    BlockLocations tbl = BLOCK_LOCATIONS__INIT;
    int i, dns[MAX_DN], n_bl;

    toret = (BlockLocations *)malloc(sizeof(BlockLocations));
    *toret = tbl;
    toret->has_blocknumber = 1;
    toret->blocknumber = blocknum;
    for(i = 0, n_bl = 0;i < MAX_DN;i++){
	if(fl_dn[i] && dnblocks[i][blocknum] == 1){
	    dns[n_bl++] = i;
	}
    }
    toret->n_locations = n_bl;
    toret->locations = malloc(sizeof(DataNodeLocation *)*n_bl);
    for(i = 0;i < n_bl;i++){
	toret->locations[i] = dnloc + dns[i];
    }

    return toret;
}

void save_state(void){
    int fd, i, j;

    fd = open("nn_save", O_CREAT | O_TRUNC | O_WRONLY, 0644);
    dprintf(fd, "%d\n", n_files);
    for(i = 0;i < n_files;i++){
	dprintf(fd, "%s %d", filenames[i], n_fileblocks[i]);
	for(j = 0;j < n_fileblocks[i];j++){
	    dprintf(fd, " %d", fileblocks[i][j]);
	}
	dprintf(fd, "\n");
    }
}

void load_state(void){
    int i, j;
    FILE *fd;

    n_files = 0;
    for(i = 0;i < MAX_DN;i++){
	fl_dn[i] = 0;
    }
    for(i = 0;i < MAX_BLK;i++){
	fl_block[i] = 0;
    }

    fd = fopen("nn_save", "r");
    if(fd == NULL){
	fprintf(stdout, "No NN save\n");
	return ;
    }
    fscanf(fd, "%d", &n_files);
    for(i = 0;i < n_files;i++){
	fscanf(fd, "%s %d", filenames[i], n_fileblocks + i);
	for(j = 0;j < n_fileblocks[i];j++){
	    fscanf(fd, "%d", fileblocks[i] + j);
	    fl_block[fileblocks[i][j]] = 1;
	}
    }
}
