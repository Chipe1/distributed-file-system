#include "xdr_proto.h"

bool_t xdr_proto(XDR *xdrs, PBW *arg){
    if(!xdr_int(xdrs, &(arg->chk))){
	perror("xdr_proto");
	return FALSE;
    }
    if(!xdr_bytes(xdrs, &(arg->s), &(arg->slen), 1024*1024*5)){
	perror("xdr_proto");
	return FALSE;
    }
    return TRUE;
}

PBW* make_pbw(char **s, int len){
    PBW *toret;
    
    toret = (PBW *)malloc(sizeof(PBW));
    toret->s = *s;
    toret->slen = len;
    toret->chk = 12^len;

    return toret;
}
